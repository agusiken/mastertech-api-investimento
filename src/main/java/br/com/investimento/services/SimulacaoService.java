package br.com.investimento.services;

import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;


    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacao = simulacaoRepository.findAll();
        return simulacao;
    }

    public void salvarSimulacao(Simulacao simulacao){
        Simulacao simul = simulacaoRepository.save(simulacao);
    }

}
