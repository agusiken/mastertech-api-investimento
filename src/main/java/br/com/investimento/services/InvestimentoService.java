package br.com.investimento.services;

import br.com.investimento.models.DTO.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;


    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodosOsInvestimentos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Investimento buscarPorId(Integer id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);

        if(optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        }
        throw new RuntimeException("O investimento não foi encontrada!");
    }


    public SimulacaoDTO executarSimulacao(Integer id, Simulacao simulacao){

        SimulacaoDTO simulacaoDTO = new SimulacaoDTO();

        double montante = simulacao.getValor();

        Investimento invest = buscarPorId(id);

        for(int i = 0; i < simulacao.getQuantidadeMeses(); i++){
            //taxa como 1.2% = 20%
            montante *= invest.getRendimentoAoMes();
        }

        simulacaoDTO.setMontante(montante);
        simulacaoDTO.setRendimentoPorMes(invest.getRendimentoAoMes());
        simulacaoService.salvarSimulacao(simulacao);

        return simulacaoDTO;
    }
}