package br.com.investimento.controllers;

import br.com.investimento.models.DTO.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimento")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    public Investimento registrarInvestimento(@RequestBody Investimento investimento) {
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return investimentoObjeto;
    }

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(@RequestParam(name = "idInvestimento", required = false) Integer id) {
        if(id != null){
            Investimento investimento = investimentoService.buscarPorId(id);
            return (Iterable)investimento;
        }
        Iterable<Investimento> investimentos = investimentoService.buscarTodosOsInvestimentos();
        return investimentos;
    }

    @PostMapping("/{id}/simulacao")
    public ResponseEntity<SimulacaoDTO> executarSimulacao(@PathVariable(name = "idInvestimento") Integer id,
                                          @RequestBody Simulacao simulacao){
        SimulacaoDTO simulacaoDTO = investimentoService.executarSimulacao(id,simulacao);

        return ResponseEntity.status(201).body(simulacaoDTO);
    }
}