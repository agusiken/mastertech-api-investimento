package br.com.investimento.repositories;

import br.com.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

}
