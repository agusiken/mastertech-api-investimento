package br.com.investimento.repositories;

import br.com.investimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SimulacaoRepository extends CrudRepository<Simulacao,Integer> {


}
