package br.com.investimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class SimulacaoInvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulacaoInvestimentoApplication.class, args);
	}

}
