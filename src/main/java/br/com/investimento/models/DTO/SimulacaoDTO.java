package br.com.investimento.models.DTO;

public class SimulacaoDTO {

    private double rendimentoPorMes;
    private double montante;


    public SimulacaoDTO(){}

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
